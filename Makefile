# =============================================================================
# Makefile to populate configuration files
#
# Author:		Khang Nguyen
# =============================================================================

.PHONY: 	all bash dircolors xinit xrandr wallpaper local vim omf compton i3 polybar dunst

all:
	echo "Populating configuration files...."
	make bash
	make dircolors
	make xinit
	make xrandr
	make wallpaper
	make local
	make vim
	make omf
	make compton
	make i3
	make polybar
	make dunst
bash:
	cp -v bash/bash_logout ~/.bash_logout
	cp -v bash/bash_profile ~/.bash_profile
	cp -v bash/bashrc ~/.bashrc
dircolors:
	cp -v dir_colors/dircolors ~/.dircolors
xinit:
	cp -v xorg/xinitrc ~/.xinitrc
	cp -v xorg/Xresources ~/.Xresources
xrandr:
	mkdir -vp ~/.screenlayout
	cp -v screenlayout/monitors.sh ~/.screenlayout/monitors.sh
wallpaper:
	mkdir -vp ~/.wallpaper
	cp -v wallpaper/* ~/.wallpaper/
local:
	mkdir -vp ~/.local/bin
	mkdir -vp ~/.local/share
	cp -vp local/bin/sp ~/.local/bin/sp
	chmod +x ~/.local/bin/sp
vim:
	git clone https://github.com/amix/vimrc.git ~/.vim_runtime
	sh ~/.vim_runtime/install_awesome_vimrc.sh
	rm -rf ~/.vim_runtime/sources_non_forked/lightline-ale && rm -rf ~/.vim_runtime/sources_non_forked/lightline.vim
	cd ~/.vim_runtime/my_plugins && \
		git clone https://github.com/vim-airline/vim-airline.git \
		git clone https://github.com/vim-airline/vim-airline-themes.git \
		git clone https://github.com/wincent/terminus.git \
		git clone https://github.com/Yggdroot/indentLine.git \
		git clone https://github.com/jistr/vim-nerdtree-tabs.git \
		git clone https://github.com/Xuyuanp/nerdtree-git-plugin.git \
		git clone https://github.com/jelera/vim-javascript-syntax.git \
		git clone https://github.com/chriskempson/base16-vim.git
	mkdir -cp ~/.vim
	cp -v vim_runtime/my_configs.vim ~/.vim_runtime/my_configs.vim
omf:
	curl -L https://get.oh-my.fish | fish
compton:
	mkdir -vp ~/.config/compton
	cp -v compton/compton.conf ~/.config/compton/compton.conf
i3:
	mkdir -vp ~/.config/i3
	cp -v i3/config ~/.config/i3/config
polybar:
	mkdir -vp ~/.config/polybar/scripts
	mkdir -vp ~/.local/share/polybar
	cp -v polybar/config ~/.config/polybar/config
	cp -v polybar/launch.sh ~/.local/share/polybar/launch.sh
	cd ~/.config/polybar/scripts && ln -s ~/.local/share/polybar/launch.sh .
dunst:
	mkdir -vp ~/.config/dunst
	cp -v dunst/dunstrc ~/.config/dunst/dunstrc
