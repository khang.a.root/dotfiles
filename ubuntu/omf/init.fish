# oh-my-fish configuration file

# -----------------------------------------------------------------------------
# GENERAL SETTINGS
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# BOBTHEFISH PROMPT SETTINGS
# -----------------------------------------------------------------------------
set -g theme_display_git yes
set -g theme_display_git_dirty yes
set -g theme_display_git_untracked yes
set -g theme_display_git_ahead_verbose yes
set -g theme_display_git_dirty_verbose yes
set -g theme_display_git_master_branch yes
set -g theme_git_worktree_support yes
set -g theme_display_date yes
set -g theme_display_cmd_duration yes
set -g theme_title_display_process no
set -g theme_date_format "+%a %H:%M"
set -g theme_avoid_ambiguous_glyphs yes
set -g theme_powerline_fonts yes
set -g theme_nerd_fonts yes
set -g theme_show_exit_status yes
set -g default_user your_normal_user
set -g theme_display_hostname yes
set -g theme_color_scheme terminal2
set -g fish_prompt_pwd_dir_length 0
set -g theme_project_dir_length 0

# -----------------------------------------------------------------------------
# Agnoster prompt settings
# -----------------------------------------------------------------------------
set -g theme_display_user yes
set -g theme_hide_hostname no
# -----------------------------------------------------------------------------
# FUNCTIONS
# -----------------------------------------------------------------------------
function ls --description "list primary contents of a directory"
  exa $argv
end

function la --description "list primary & hidden contents of a directory"
  exa -a $argv
end

function lsl --description "list primary contents metadata of a directory"
  exa -lh $argv
end

function lal --description "list primary & hidden contents + metadata"
  exa -lah $argv
end

function lst --description "list primary contents in tree view"
  exa --tree
end

function bmontop
  bmon -p enp0s31f6 -o curses
end

function neo --description "matrix terminal saver"
  cmatrix -sb -u 6
end

function grab
grep --ignore-case --recursive --context=3 $argv
end

function home --description "navigate to home directory"
  cd $HOME
end

function clr
clear
fortune -s
end

function fish_greeting
  set_color $fish_color_autosuggestion
  fortune -s
  set_color normal
end

function globalprotect
    sudo openconnect --protocol=gp gp.cs.odu.edu --dump -vvv
end

function glancestop
  glances --percpu --time 2 --disable-plugin mem,memswap,load --disable-left-sidebar --process-short-name
end
#------------------------------------------------------------------------------
