;=====================================================
;
;   To learn more about how to configure Polybar
;   go to https://github.com/jaagr/polybar
;
;   The README contains alot of information
;
;=====================================================

[colors]
background = ${xrdb:background}
background-alt = ${xrdb:color7}

foreground = ${xrdb:foreground}
foreground-alt = ${xrdb:color0}

black = ${xrdb:color0}
gray = ${xrdb:color8}
white = ${xrdb:color15}

red = ${xrdb:color1}
orange = ${xrdb:color16}
green = ${xrdb:color2}
yellow = ${xrdb:color3}
blue = ${xrdb:color4}
magenta = ${xrdb:color5}
cyan = ${xrdb:color6}
alert = ${colors.red}


;==============================================================================

[bar/middle]
bottom = false
monitor = HDMI1
;monitor = ${env:MONITOR}
width = 100%
height = 24
;offset-x = 1%
;offset-y = 1%
fixed-center = true

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 2
border-size = 2
border-color = ${colors.black}

padding-left = 2
padding-right = 2

module-margin-left = 2
module-margin-right = 2

font-0 = "NotoSansDisplay Nerd Font:style=SemiCondensed Medium,Regular:pixelsize=11;1:antialias=true"
font-1 = "Font Awesome 5 Free:style=Regular:pixelsize=11,0:antialias=true"
font-2 = "Font Awesome 5 Free:style=Solid:pixelsize=11,0:antialias=true"
font-3 = "Font Awesome 5 Brands:style=Regular:pixelsize=11,0:antialias=true"
font-4 = "Font Awesome 5 Brands:style=Solid:pixelsize=11,0:antialias=true"
font-5 = "Weather Icons:style=Regular:size=11;1"
;font-1 = "FontAwesome:size=10;1:antialias=true"

modules-left = i3
modules-center = sep time sep
modules-right = netspeedup netspeeddown core0 core1 core2 core3 cpu memory sep powermenu

;wm-restack = bspwm
;wm-restack = i3

;override-redirect = true

cursor-click = pointer
cursor-scroll = ns-resize

enable-ipc = true

;===============================================================================
[bar/left]
inherit = bar/middle
monitor = DP2

modules-left = i3
modules-center = date
modules-right = updates sep weather sep

;===============================================================================
[bar/right]
inherit = bar/middle
monitor = DP1

modules-left = i3
modules-center = sep time sep
modules-right =

tray-position = right
tray-padding = 4
tray-background = ${colors.black}
;===============================================================================

[module/xwindow]
type = internal/xwindow
label = %title:0:30:...%

;==============================================================================

[module/updates]
type = custom/script
exec = ~/.config/polybar/scripts/updates-arch-combined.sh
interval = 300
format-padding = 2
line-size = 1
format-underline = ${colors.red}

;==============================================================================

[module/netspeedup]
type = internal/network
interface = enp0s31f6
interval = 1.0
accumulate-stats = true

label-connected = " %upspeed:2%"
format-connected = <label-connected>
format-connected-prefix = " "
format-connected-prefix-foreground = ${colors.red}
format-connected-foreground = ${colors.foreground}
format-connected-background = ${colors.background}

;==============================================================================

[module/netspeeddown]
type = internal/network
interface = enp0s31f6
interval = 1.0
accumulate-stats = true

label-connected = " %downspeed:2%"
format-connected = <label-connected>
format-connected-prefix = " "
format-connected-prefix-foreground = ${colors.green}
format-connected-foreground = ${colors.foreground}
format-connected-background = ${colors.background}

;==============================================================================
; PolyNews
;==============================================================================
[module/polynews]
type = custom/script
exec = python ~/.config/polybar/scripts/polynews/theguardian.py
interval = 30
format-prefix = "  "

;==============================================================================
; Weather Bar
;==============================================================================
[module/weather]
type = custom/script
exec = ~/.config/polybar/scripts/openweathermap-fullfeatured.sh
interval = 600
label-font = 6

;==============================================================================
; Polybar Spotify Controls
;==============================================================================

[module/previous]
type = custom/script
interval = 86400

format = "%{T3}<label>"
format-padding = 5
format-underline = ${colors.green}
line-size = 2

; previous song icon
exec = echo ""
click-left = "dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Previous"

;------------------------------------------------------------------------------

[module/next]
type = custom/script
interval = 86400

format = "%{T3}<label>"
format-padding = 5
format-underline = ${colors.green}
line-size = 2

exec = echo ""
click-left = "dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Next"

;------------------------------------------------------------------------------

[module/playpause]
type = custom/ipc

format-underline = ${colors.green}
line-size = 2

; default
hook-0 = echo ""
; playing
hook-1 = echo ""
; paused
hook-2 = echo ""

initial = 1

click-left = "dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.PlayPause"

;------------------------------------------------------------------------------

[module/spotify]
type = custom/script

format-padding = 4

hook-0 = echo ""
hook-1 = python3 ~/.config/polybar/scripts/spotify/spotify_status.py

;==============================================================================
[module/win10]
type = custom/text
content = 
content-foreground = ${colors.cyan}
content-background = ${colors.background}
content-padding = 1

[module/sep]
type = custom/text
content = |
content-padding = 0
content-foreground = ${colors.foreground}
content-background = ${colors.background}

;==============================================================================

[module/xkeyboard]
type = internal/xkeyboard
blacklist-0 = num lock

format-prefix = "   "
format-prefix-foreground = ${colors.foreground}
format-prefix-underline = ${colors.blue}

label-layout = "%layout% "
label-layout-underline = ${colors.blue}

label-indicator-padding = 2
label-indicator-margin = 0
label-indicator-background = ${colors.background}
label-indicator-underline = ${colors.blue}

;==============================================================================

[module/filesystem]
type = internal/fs
interval = 30

mount-0 = /
format-mounted-prefix = " "
format-mounted-prefix-background = ${colors.background}
format-mounted-prefix-foreground = ${colors.yellow}
label-mounted = " disk %percentage_used%%"
label-unmounted = %mountpoint% not mounted
label-unmounted-foreground = ${colors.foreground-alt}

;==============================================================================

[module/bspwm]
type = internal/bspwm

label-focused = %index%
label-focused-background = ${colors.background}
label-focused-foreground = ${colors.cyan}
label-focused-underline = ${colors.cyan}
label-focused-padding = 2

label-unfocused-background = ${colors.background}
label-unfocused-foreground = ${colors.foreground}
label-unfocused-underline = ${colors.background-alt}
label-occupied = %index%
label-occupied-padding = 2

label-urgent = %index%
label-urgent-background = ${colors.background}
label-urgent-foreground = ${colors.red}
label-urgent-underline = ${colors.red}
label-urgent-padding = 2

label-empty = %index%
label-empty-foreground = ${colors.foreground}
label-empty-padding = 2

;==============================================================================

[module/i3]
type = internal/i3
format = <label-state> <label-mode>
index-sort = true
wrapping-scroll = true

; Only show workspaces on the same output as the bar
pin-workspaces = true

; Separator in between workspaces
label-separator = 
label-separator-padding = 2
label-separator-foreground = ${colors.gray}
label-separator-background = ${colors.background}

# HP LA2405x monitor (left)
ws-icon-0 = "1; desktop  "
ws-icon-1 = "2; music  "
ws-icon-2 = "3; workspace  "

ws-icon-3 = "4; blackboard  "
ws-icon-4 = "5; chromium  "
ws-icon-5 = "6; workspace  "
ws-icon-6 = "7; workspace  "

ws-icon-7 = "8; workspace  "
ws-icon-8 = "9; Google Drive  "
ws-icon-9 = "10; remote desktop  "

label-mode-padding = 2
label-mode-foreground = ${colors.foreground}
label-mode-background = ${colors.background}

; focused = Active workspace on focused monitor
label-focused = "%index%: %icon%"
label-focused-background = ${module/bspwm.label-focused-background}
label-focused-foreground = ${module/bspwm.label-focused-foreground}
label-focused-underline = ${module/bspwm.label-focused-underline}
label-focused-padding = ${module/bspwm.label-focused-padding}

; unfocused = Inactive workspace on any monitor
label-unfocused = "%index%: %icon%"
label-unfocused-background = ${module/bspwm.label-unfocused-background}
label-unfocused-foreground = ${module/bspwm.label-unfocused-foreground}
label-unfocused-underline = ${xrdb:color0}
label-unfocused-padding = ${module/bspwm.label-occupied-padding}

; visible = Active workspace on unfocused monitor
label-visible = "%index%: %icon%"
label-visible-background = ${module/bspwm.label-unfocused-background}
label-visible-foreground = ${module/bspwm.label-unfocused-foreground}
label-visible-underline = ${xrdb:color7}
label-visible-padding = ${self.label-unfocused-padding}

; urgent = Workspace with urgency hint set
label-urgent = "%index%: %icon%"
label-urgent-background = ${module/bspwm.label-urgent-background}
label-urgent-foreground = ${module/bspwm.label-urgent-foreground}
label-urgent-underline = ${module/bspwm.label-urgent-underline}
label-urgent-padding = ${module/bspwm.label-urgent-padding}

;==============================================================================

[module/mpd]
type = internal/mpd
format-online = <label-song>  <icon-prev> <icon-stop> <toggle> <icon-next>

icon-prev = ""
icon-stop = ""
icon-play = ""
icon-pause =""
icon-next = ""

label-song-maxlen = 25
label-song-ellipsis = true

;==============================================================================

[module/xbacklight]
type = internal/xbacklight

format = <label> <bar>
label = BL

bar-width = 10
bar-indicator = |
bar-indicator-foreground = #ff
bar-indicator-font = 2
bar-fill = ─
bar-fill-font = 2
bar-fill-foreground = #9f78e1
bar-empty = ─
bar-empty-font = 2
bar-empty-foreground = ${colors.foreground-alt}

;==============================================================================

[module/backlight-acpi]
inherit = module/xbacklight
type = internal/backlight
card = intel_backlight

;==============================================================================

[module/cpu]
type = internal/cpu
interval = 1
format-prefix = " "
format-prefix-background = ${colors.background}
format-prefix-foreground = ${colors.blue}
label = " cpu %percentage:1%%"

;==============================================================================

[module/memory]
type = internal/memory
interval = 3

label = " mem %percentage_used%%"
format-prefix = " "
format-prefix-foreground = ${colors.magenta}
;==============================================================================

[module/wlan]
type = internal/network
interface = wlp4s0
interval = 3.0

format-connected = <ramp-signal> <label-connected>
format-connected-underline = #859900
label-connected = %essid%

;format-disconnected =
format-disconnected = <label-disconnected>
format-disconnected-underline = #657b83
label-disconnected = %ifname%
;label-disconnected-foreground = ${colors.foreground-alt}

ramp-signal-0 = "  "
ramp-signal-foreground = ${colors.foreground-alt}

[module/eth]
type = internal/network
interface = enp0s31f6
interval = 3.0

format-connected-underline = ${colors.yellow}
format-connected-prefix = " "
format-connected-prefix-foreground = ${colors.foreground}
label-connected = " %local_ip% "

;format-disconnected =
format-disconnected = <label-disconnected>
format-disconnected-underline = ${colors.red}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${colors.foreground-alt}

;==============================================================================

[module/time]
type = internal/date
interval = 1
time = "%I:%M:%S %p"
format-prefix-foreground = ${colors.foreground}
label = "%time% "

;==============================================================================

[module/date]
type = internal/date
interval = 60

date = "%A, %B %d"
date-alt = "%Y-%m-%d"

format =   <label>

label = "%date%"

;==============================================================================

[module/pulseaudio]
type = internal/pulseaudio

format-volume = <label-volume> <bar-volume>
label-volume = "   vol %percentage%%"
label-volume-foreground = ${colors.foreground}

label-muted = "   vol %percentage%%"
label-muted-foreground = #66

bar-volume-width = 10
bar-volume-foreground-0 = #586e75
bar-volume-foreground-1 = #657b83
bar-volume-foreground-2 = #657b83
bar-volume-foreground-3 = #839496
bar-volume-foreground-4 = #839496
bar-volume-foreground-5 = #93a1a1
bar-volume-foreground-6 = #93a1a1
bar-volume-gradient = false
bar-volume-indicator = |
bar-volume-indicator-font = 3
bar-volume-fill = ─
bar-volume-fill-font = 3
bar-volume-empty = ─
bar-volume-empty-font = 3
bar-volume-empty-foreground = ${xrdb:color0:#222}

[module/alsa]
type = internal/alsa

format-volume = <label-volume> <bar-volume>
label-volume = VOL
label-volume-foreground = ${root.foreground}

format-muted-prefix = " "
format-muted-foreground = ${colors.foreground-alt}
label-muted = sound muted

bar-volume-width = 10
bar-volume-foreground-0 = #55aa55
bar-volume-foreground-1 = #55aa55
bar-volume-foreground-2 = #55aa55
bar-volume-foreground-3 = #55aa55
bar-volume-foreground-4 = #55aa55
bar-volume-foreground-5 = #f5a70a
bar-volume-foreground-6 = #ff5555
bar-volume-gradient = false
bar-volume-indicator = |
bar-volume-indicator-font = 2
bar-volume-fill = ─
bar-volume-fill-font = 2
bar-volume-empty = ─
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.foreground-alt}

;==============================================================================

[module/battery]
type = internal/battery
battery = BAT0
adapter = AC
full-at = 98

format-charging = <animation-charging> <label-charging>
format-charging-underline = #859900

format-discharging = <ramp-capacity> <label-discharging>
format-discharging-underline = dc322f

format-full-prefix = " "
format-full-prefix-foreground = ${colors.foreground-alt}
format-full-underline = ${self.format-charging-underline}

ramp-capacity-0 = " "
ramp-capacity-1 = " "
ramp-capacity-2 = " "
ramp-capacity-3 = " "
ramp-capacity-foreground = ${colors.foreground-alt}

animation-charging-0 = " "
animation-charging-1 = " "
animation-charging-2 = " "
animation-charging-3 = " "
animation-charging-4 = " "
animation-charging-foreground = ${colors.foreground-alt}
animation-charging-framerate = 750

;==============================================================================

[module/core0]
type = internal/temperature
interval = 1.0
thermal-zone = 0
warn-temperature = 60
hwmon-path = /sys/devices/platform/coretemp.0/hwmon/hwmon1/temp2_input

format = "<ramp>  <label>"
format-warn = "<ramp>  <label-warn>"
format-warn-background = ${colors.alert}
format-warn-foreground = ${colors.background}
format-warn-underline = ${colors.background}

label = "%temperature-c%"
label-warn = %temperature-c%
label-warn-background = ${colors.alert}
label-warn-foreground = ${colors.background}

ramp-0 = ""
ramp-1 = ""
ramp-2 = ""
ramp-3 = ""
ramp-4 = ""
ramp-foreground = ${colors.yellow}

;==============================================================================

[module/core1]
type = internal/temperature
interval = 1.0
thermal-zone = 0
warn-temperature = 60
hwmon-path = /sys/devices/platform/coretemp.0/hwmon/hwmon1/temp3_input

format = " <label>"
format-warn = " <label-warn>"
format-warn-background = ${colors.alert}
format-warn-foreground = ${colors.background}
format-warn-underline = ${colors.background}

label = "%temperature-c%"
label-warn = %temperature-c%
label-warn-background = ${colors.alert}
label-warn-foreground = ${colors.background}

;==============================================================================

[module/core2]
type = internal/temperature
interval = 1.0
thermal-zone = 0
warn-temperature = 60
hwmon-path = /sys/devices/platform/coretemp.0/hwmon/hwmon1/temp4_input

format = " <label>"
format-warn = " <label-warn>"
format-warn-background = ${colors.alert}
format-warn-foreground = ${colors.background}
format-warn-underline = ${colors.background}

label = "%temperature-c%"
label-warn = %temperature-c%
label-warn-background = ${colors.alert}
label-warn-foreground = ${colors.background}

;==============================================================================

[module/core3]
type = internal/temperature
interval = 1.0
thermal-zone = 0
warn-temperature = 60
hwmon-path = /sys/devices/platform/coretemp.0/hwmon/hwmon1/temp5_input

format = " <label>"
format-warn = " <label-warn>"
format-warn-background = ${colors.alert}
format-warn-foreground = ${colors.background}
format-warn-underline = ${colors.background}

label = "%temperature-c%"
label-warn = %temperature-c%
label-warn-background = ${colors.alert}
label-warn-foreground = ${colors.background}

;==============================================================================

[module/powermenu]
type = custom/menu

expand-right = true

format-spacing = 1

label-open = 
label-open-foreground = ${colors.cyan}
label-close = " cancel"
label-close-foreground = ${colors.foreground}
label-separator = |
label-separator-foreground = ${colors.foreground}

menu-0-0 = reboot
menu-0-0-exec = menu-open-1
menu-0-1 = power off
menu-0-1-exec = menu-open-2

menu-1-0 = cancel
menu-1-0-exec = menu-open-0
menu-1-1 = reboot
menu-1-1-exec = sudo reboot

menu-2-0 = power off
menu-2-0-exec = sudo poweroff
menu-2-1 = cancel
menu-2-1-exec = menu-open-0

[settings]
screenchange-reload = true
;compositing-background = compton
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over
throttle-output = 5
throttle-output-for = 10
throttle-input-for = 30

[global/wm]
margin-top = 0
margin-bottom = 0

; vim:ft=dosini
