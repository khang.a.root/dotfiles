#!/usr/bin/env sh

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -x polybar >/dev/null; do sleep 5; done

# Launch bar1 and bar2
MONITOR=DP-2 polybar top &
MONITOR=HDMI-1 polybar top &
MONITOR=DP-1 polybar top

echo "Bars launched..."
