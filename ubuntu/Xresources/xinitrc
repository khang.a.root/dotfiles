#!/bin/sh
#
#
# Author:   Khang M. Nguyen
# OS:       Arch Linux
# ==============================================================================

# ------------------------------------------------------------------------------
# local variables
# ------------------------------------------------------------------------------
USER_RESOURCES=$HOME/.Xresources
USER_XMODMAP=$HOME/.Xmodmap
SYS_RESOURCES=/etc/X11/xinit/.Xresources
SYS_XMODMAP=/etc/X11/xinit/.Xmodmap

XRANDR=$HOME/.screenlayout/monitors.sh
COMPTON=$HOME/.config/compton/compton.conf
#ICC=$HOME/.local/share/icc/hp-la2450x.icc

BG_LEFT=$HOME/.wallpaper/bg_left
BG_MIDDLE=$HOME/.wallpaper/bg_middle
BG_RIGHT=$HOME/.wallpaper/bg_right
# ------------------------------------------------------------------------------

# run all system xinitrx shell scripts
if [ -d /etc/X11/xinit/xinitrc.d ] ; then
 for f in /etc/X11/xinit/xinitrc.d/?*.sh ; do
  [ -x "$f" ] && . "$f"
 done
 unset f
fi

# launch dbus
if [ -n $DBUS_SESSION_BUS_ADDRESS ]; then
    export $(dbus-launch)
fi

# launch pulseaudio as background process
#if [ -x /usr/bin/pulseaudio ]; then
#    /usr/bin/pulseaudio --start
#fi
# register pulseaudio
#if [ -x /usr/bin/start-pulseaudio-x11 ]; then
#    /usr/bin/start-pulseaudio-x11
#fi

# user preference utility for X
if [ -f /usr/bin/xset ]; then
    /usr/bin/xset -b
    /usr/bin/xset s 3600 3600
fi

# launch monitor configuration
if [ -f /usr/bin/xrandr ]; then
    $XRANDR
fi

# launch window compositor
if [ -f /usr/bin/compton ]; then
    /usr/bin/compton -CG --config $COMPTON --daemon
fi

# calibrate display
#if [ -f /usr/bin/xcalib ]; then
#  /usr/bin/xcalib -d :0 $ICC
#fi

# load system xresources
if [ -f $SYS_RESOURCES ]; then
  /usr/bin/xrdb -merge $SYS_RESOURCES &
fi

# load system keymaps
if [ -f $SYS_XMODMAP ]; then
  /usr/bin/xmodmap $SYS_XMODMAP
fi

# load user resources
if [ -f "$USER_RESOURCES" ]; then
  /usr/bin/xrdb -merge "$USER_RESOURCES" &
fi

# load user kepmaps
if [ -f "$USER_XMODMAP" ]; then
    /usr/bin/xmodmap "$USER_XMODMAP"
fi

# display wallpapers
if [ -f /usr/bin/feh ]; then
    /usr/bin/feh --bg-scale $BG_RIGHT --bg-scale $BG_MIDDLE --bg-scale $BG_LEFT
fi

# launch notification daemon
if [ -f /usr/bin/dunst ]; then
    /usr/bin/dunst -config $HOME/.config/dunst/dunstrc &
fi

# launch ssh authentication agent
if [ -f /usr/bin/ssh-agent ]; then
    eval $(ssh-agent)
fi

# launch I3 window manager
exec i3
