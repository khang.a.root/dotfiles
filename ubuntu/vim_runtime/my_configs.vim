" *****************************************************************************
" /home/khang/.vim_runtime/my_configs.vim
"
" personal settings for vim
" *****************************************************************************
"
" ========== general settings ==========

set history=500     " line history
set mouse=a         " enable mouse support
filetype plugin on  " filetype plugins
set nocompatible
set colorcolumn=80
set ruler

" ----- tab & indent -----
filetype indent on " turn off auto indent
set expandtab       " use spaces instead of tabs
set smarttab        " be smart
set shiftwidth=2    " 2 spaces == 1 tab
set tabstop=2
set autoindent
set nosmartindent


" =========== display settings ==========

" ----- general -----
set number          " set line numbers
syntax enable       " syntax highlighting
set nowrap          " no text wrapping
set foldcolumn=1    " no left margin
set cursorline
let base16colorspace=256
set background=dark
colorscheme base16-default-dark

" ----- airline status -----
set cmdheight=1
set laststatus=2
let g:airline_theme='base16'
let g:airline#extensions#tabline#enabled=1
let g:airline#extensions#whitespace#enabled = 1
let g:airline_powerline_fonts=1

" ----- indent line -----
let g:indentLine_enabled=1
let g:indentLine_color_tty_dark=1
let g:indentLine_char = '¦'

" ----- better whitespace ------
let g:better_whitespace_enabled=1
let g:strip_whitespace_on_save=1
let g:strip_whitelines_at_eof=1
let g:better_whitespace_skip_empty_lines=1

" ----- YouCompleteMe settings -----

" java
let g:syntastic_java_checkers = []
let g:EclimFileTypeValidate = 0

" ------ NERD commenter -----
let g:NERDSpaceDelims = 1
let g:NERDCompactSexyComs = 1
let g:NERDDefaultAlign = 'left'
let g:NERDAltDelims_java = 1
let g:NERDTrimTrailingWhitespace = 1
let g:NERDToggleCheckAllLines = 1

" ----- Vim Markdown -----
let g:vim_markdown_folding_disabled = 1

" ----- AutoPairs -----
let g:AutoPairsFlyMode = 0

" ----- NERD Tree -----

" <Ctrl-n> to open NERD Tree
map <C-a> :NERDTreeToggle<CR>

" close vim if only NERDTree is left open
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" NERD Tree on left
let g:NERDTreeWinPos = 'left'

" ===========================================
