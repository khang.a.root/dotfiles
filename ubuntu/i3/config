# *****************************************************************************
# /home/khang/.config/i3/config
#
#   - i3 desktop manager configuration
#
# author:    khang nguyen
# *****************************************************************************

# ====================================================
# ================== key bindings ====================
# ====================================================

# ALT superkey
set $mod Mod1

# no titles
new_window pixel

# use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# kill focused window
bindsym $mod+Shift+q kill

# switch to workspace
bindsym $mod+1 workspace 1
bindsym $mod+2 workspace 2
bindsym $mod+3 workspace 3
bindsym $mod+4 workspace 4
bindsym $mod+5 workspace 5
bindsym $mod+6 workspace 6
bindsym $mod+7 workspace 7
bindsym $mod+8 workspace 8
bindsym $mod+9 workspace 9
bindsym $mod+0 workspace 10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace 1
bindsym $mod+Shift+2 move container to workspace 2
bindsym $mod+Shift+3 move container to workspace 3
bindsym $mod+Shift+4 move container to workspace 4
bindsym $mod+Shift+5 move container to workspace 5
bindsym $mod+Shift+6 move container to workspace 6
bindsym $mod+Shift+7 move container to workspace 7
bindsym $mod+Shift+8 move container to workspace 8
bindsym $mod+Shift+9 move container to workspace 9
bindsym $mod+Shift+0 move container to workspace 10

# change focus
bindsym $mod+j focus left
bindsym $mod+k focus down
bindsym $mod+l focus up
bindsym $mod+odiaeresis focus right
# alternatively, use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+j move left
bindsym $mod+Shift+k move down
bindsym $mod+Shift+l move up
bindsym $mod+Shift+odiaeresis move right
# alternatively, use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+h split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# reload the configuration file
bindsym $mod+Shift+c reload

# restart i3 inplace
bindsym $mod+Shift+r restart

bindsym $mod+Shift+e exec "i3-msg exit"

# resize window
mode "resize" {
        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym j resize shrink width 2 px or 2 ppt
        bindsym k resize grow height 2 px or 2 ppt
        bindsym l resize shrink height 2 px or 2 ppt
        bindsym odiaeresis resize grow width 2 px or 2 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 2 px or 2 ppt
        bindsym Down resize grow height 2 px or 2 ppt
        bindsym Up resize shrink height 2 px or 2 ppt
        bindsym Right resize grow width 2 px or 2 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

bindsym $mod+r mode "resize"

# ========== Application Bindings ==========

# ----- termite ------
bindsym $mod+Return exec --no-startup-id termite

# ----- rofi -----
bindsym $mod+d exec --no-startup-id rofi -show drun -show-icons -drun-icon-theme Papirus-Dark -hide-scrollbar -lines 7 -tokenize -padding 10 -font 'Noto Sans Display Medium 12'

# ----- lock screen -----
bindsym $mod+ctrl+l exec --no-startup-id i3lock-fancy -f "Noto Sans Display 12" -t "Does the Liar guard the Door to Paradise?" -- scrot -z && systemctl suspend

# ----- spotify controls -----
bindsym $mod+greater exec --no-startup-id sp next
bindsym $mod+less exec --no-startup-id sp prev
bindsym $mod+slash exec --no-startup-id sp play

# ========== windows settings ==========

# ----- borders -----
for_window [class="^.*"] border pixel 2
gaps inner 10

# ========== Application Startup Settings ==========

font pango:NotoSansDisplay Nerd Font Medium 10
focus_follows_mouse yes

# ----- monitors -----

# hdmi-1 monitor (left)
workspace 1 output DP2
workspace 2 output DP2
workspace 3 output DP2

workspace 4 output HDMI1
workspace 5 output HDMI1
workspace 6 output HDMI1
workspace 7 output HDMI1

workspace 8 output DP1
workspace 9 output DP1
workspace 10 output DP1

# ----- color settings -----
set_from_resource $black color0 #1b2b34
set_from_resource $fore color7 #c0c5ce
set_from_resource $red color1 #99c794
set_from_resource $cyan color6 #5fb3b3
set_from_resource $gray color8 #65737e
set_from_resource $white color15 #d8dee9

client.focused $cyan $cyan $fore $cyan
client.focused_inactive  $gray $gray $fore $gray
client.unfocused $gray $gray $fore $gray
client.urgent $red $red $fore $red


# ----- polybar -----
exec_always --no-startup-id "$HOME/.config/polybar/scripts/launch.sh &"

# ----- dropbox -----
exec --no-startup-id dropbox start &
